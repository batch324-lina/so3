<?php
class Person{
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printname(){
        return "Your full name is $this->firstName $this->middleName $this->lastName.";
    }
}

class Developer extends Person{
    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName(){
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer";
    }
}

class Engineer extends Person{
    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName(){
        return "You are an engineer name $this->firstName $this->middleName $this->lastName";
    }
}

$personName = new Person('Ishigami', 'Will of D', 'Senku');
$developerName = new Developer('John', 'Finch', 'Smith');
$engineerName = new Engineer('Harold', 'Myers', 'Reese');