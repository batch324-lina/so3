<?php
require_once './code.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03 activity</title>
</head>
<body>
    <h1>Person</h1>
    <p><?php echo $personName->printName(); ?></p>
    <h1>Developer</h1>
    <p><?php echo $developerName->printName(); ?></p>
    <h1>Engineer</h1>
    <p><?php echo $engineerName->printName(); ?></p>
</body>
</html>